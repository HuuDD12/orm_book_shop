import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { BullModule } from '@nestjs/bull';
import { HttpModule } from '@nestjs/axios';
import { AppService } from './app.service';
import { JwtMiddleware } from './middlewares/JwtMiddleware';
import { DatabaseModule } from './config/database.module';
import { DatabaseCmsModule } from './config/database.auth.module';
import { JwtService } from '@nestjs/jwt';
import { AuthModule } from './modules/auth/auth.module';
import { ProductModule } from './modules/shop/product.module';

@Module({
  imports: [

    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    ConfigModule.forRoot({ isGlobal: true }),
    DatabaseModule,
    DatabaseCmsModule,
    HttpModule,
    AuthModule,
    ProductModule,
  ],
  controllers: [AppController],
  providers: [AppService,JwtService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer
      .apply(JwtMiddleware)
      .forRoutes({ path: '/', method: RequestMethod.ALL });
  }
}

