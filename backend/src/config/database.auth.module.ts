import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleEntity } from 'src/entities/auth/role.entity';
import { UserEntity } from 'src/entities/auth/user.entity';
import { UserRoleEntity } from 'src/entities/auth/user.role.entity';
@Module({
    imports: [
      ConfigModule,
      TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        name: 'auth',
        useFactory: (configService: ConfigService) => ({
          type: 'mysql',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASS'),
          database: configService.get('DB_NAME2'),
          entities: [UserEntity,RoleEntity,UserRoleEntity],
          synchronize: false,
        }),
      }),
    ],
  })
export class DatabaseCmsModule {}
