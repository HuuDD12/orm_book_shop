import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from 'src/entities/shop/ProductEntity';
@Module({
    imports: [
      ConfigModule,
      TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        name: 'shop',
        useFactory: (configService: ConfigService) => ({
          type: 'mysql',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASS'),
          database: configService.get('DB_NAME'),
          entities: [ProductEntity],
          synchronize: false,
        }),
      }),
    ],
  })
  export class DatabaseModule {}
