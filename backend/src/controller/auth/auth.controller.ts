import { Body, Controller, Post } from "@nestjs/common";
import { AuthService } from "src/service/auth/auth.service";

@Controller('auth')
export class AuthController{
    constructor(private readonly authservice: AuthService ){}
    @Post('login')
    login(@Body() dto: any){
        return this.authservice.login(dto);
    }
    @Post('register')
    register(@Body() dto: any){
        return this.authservice.register(dto);
    }

    
}