import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, HttpException, HttpStatus, Request, UseInterceptors, UploadedFile } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from '../../dto/auth/user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { storageConfig } from '../../middlewares/multer.middleware';
import { extname } from 'path';
import { UserService } from '../../service/auth/users.service';
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Post()
  @UseInterceptors(FileInterceptor('avatar', {
    storage: storageConfig(`user`),
    fileFilter: (req, file, cb) => {
      const ext = extname(file.originalname);
      const allowed = [`.jpg`, `.png`, `.jpeg`];
      if (!allowed.includes(ext)) {
        req.fileValidationError = `wrong type `;
        cb(null, false);
      } else {
        const size = parseInt(req.headers['content-lenght']);
        if (size > 1024 * 1024 * 5) {
          req.fileValidationError = ` size to big`;
          cb(null, false);
        } else {
          cb(null, true);
        }
      }
    }
  })) // Tên của trường chứa ảnh trong form data
  create(@UploadedFile() file: Express.Multer.File, @Body() createUserDto: CreateUserDto) {
    console.log(file);
    return this.userService.create(createUserDto, file?.originalname);
  }
  @Get()
  async findAll() {
    try {
      return await this.userService.findAll();
    } catch (error) {
      console.error(error);
      throw new HttpException('check', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @Get('/search')
  async searchAll(@Request() req: any) {
    try {
      const { q } = req.query;
      return await this.userService.searchAll(q);
    } catch (error) {
      console.error(error);
      throw new HttpException('check', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.userService.remove(id);
  }
}