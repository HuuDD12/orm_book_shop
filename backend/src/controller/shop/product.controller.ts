import { Body, Controller, Get, Post } from "@nestjs/common";
import { ProductService } from "../../service/shop/product.service";

@Controller('product')
export class ProductController{
    constructor(private readonly productservice: ProductService ){}
    @Get()
    getAll(){
        return this.productservice.getAll();
    }
}