export class CreateUserDto{
    avatar: string;
    firstName: string; 
    lastName: string; 
    age: number;
    date: Date; 
    address: string; 
    email: string;
    password: string;
    role: [];
}

export class UpdateUserDto{
    firstName: string; 
    lastName: string; 
    age: number;
    date: Date; 
    address: string; 
    email: string;
}