import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { UserRoleEntity } from "./user.role.entity";

@Entity({name: 'role'})
export class RoleEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ length: 100, nullable: false, unique: true, })
    roleName: string
    
    @OneToMany(() => UserRoleEntity, userRole => userRole.role)
    userRoles: UserRoleEntity[];
}