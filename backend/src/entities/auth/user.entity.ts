import { Entity, PrimaryGeneratedColumn, Column , OneToMany} from "typeorm"
import { UserRoleEntity } from "./user.role.entity"

@Entity({name: 'user'})
export class UserEntity{
    @PrimaryGeneratedColumn()
    id: number
    @Column({nullable:true})
    avatar: string
    @Column({ nullable: true, length: 100 })
    firstName: string

    @Column({ nullable: true, length: 100 })
    lastName: string
    
    @Column({ nullable: true })
    age: number

    @Column({ type: 'date', nullable: true })
    date: Date
    @Column({ nullable: true })
    address: string
    @Column({ default: 1 }) // Giá trị mặc định là 1
    isActive: number;
    @Column({ nullable: false, unique: true, length: 255 })
    email: string
    @Column({  nullable: false, length: 255 })
    password: string
    @OneToMany(() => UserRoleEntity, userRole => userRole.user)
    userRoles: UserRoleEntity[];

}