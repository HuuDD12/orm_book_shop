import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm";
import { UserEntity } from "./user.entity";
import { RoleEntity } from "./role.entity";


@Entity({ name: 'user_role' })
export class UserRoleEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => UserEntity, user => user.userRoles)
    @JoinColumn({ name: "user" })
    user: UserEntity;

    @ManyToOne(() => RoleEntity, role => role.userRoles)
    @JoinColumn({ name: "role" })
    role: RoleEntity;
    
}