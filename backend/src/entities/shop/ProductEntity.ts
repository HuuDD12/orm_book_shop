import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: 'product' })
export class ProductEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    coverImg: string;
    
    @Column({nullable: true})
    name: string;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true })
    descript: string;

    @Column({ nullable: true })
    price: number;

    @Column({ nullable: true })
    quantity: number;
}