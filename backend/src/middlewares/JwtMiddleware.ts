import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) { }
  async use(req: any, res: any, next: (error?: any) => void) {
    const authHeader = req.headers.authorization;

    if (authHeader && authHeader.split(' ')[0] === 'Bearer') {
      const token = authHeader.split(' ')[1];
      try {
      const payload = await this.jwtService.verifyAsync(
        token,
        {
          secret: `${process.env.JWT_SECRET}`,
        }
      );
        next();
        req.user = payload;
      } catch (error) {
        throw new UnauthorizedException('Invalid token');
      }
    } else {
      throw new UnauthorizedException('Missing token');
    }
  }
  // async canActivate(context: ExecutionContext): Promise<boolean> {
  //   const request = context.switchToHttp().getRequest();
  //   const token = this.extractTokenFromHeader(request);
  //   if (!token) {
  //     throw new UnauthorizedException();
  //   }
  //   try {
  //     const payload = await this.jwtService.verifyAsync(
  //       token,
  //       {
  //         secret: `${process.env.JWT_SECRET}`,
  //       }
  //     );
  //     // 💡 We're assigning the payload to the request object here
  //     // so that we can access it in our route handlers
  //     request['user'] = payload;
  //   } catch {
  //     throw new UnauthorizedException();
  //   }
  //   return true;
  // }

  // private extractTokenFromHeader(request: Request): string | undefined {
  //   const [type, token] = request.headers.authorization?.split(' ') ?? [];
  //   return type === 'Bearer' ? token : undefined;
  // }
}