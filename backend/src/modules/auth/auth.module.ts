import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
// import DatabaseCmsModule from '../../config/database.cms.module';
import { ConfigModule } from '@nestjs/config';
import { UserEntity } from '../../entities/auth/user.entity';
import { UserRepository } from '../../repositories/auth/user.repository';
import { UserService } from '../../service/auth/users.service';
import { RoleEntity } from '../../entities/auth/role.entity';
import { UserRoleEntity } from '../../entities/auth/user.role.entity';
import { AuthController } from '../../controller/auth/auth.controller';
import { AuthService } from '../../service/auth/auth.service';
import { UserRoleRepository } from '../../repositories/auth/user.role.repository';
import { RoleRepository } from '../../repositories/auth/role.repository';
import { UsersModule } from './users.module';

@Module({
  providers: [AuthService,
    JwtService,
    UserRepository,
    UserRoleRepository,
    RoleRepository,
  ],
  imports: [

    JwtModule,
    TypeOrmModule.forFeature(
      [
        UserEntity,
        UserRoleEntity,
        RoleEntity,
      ], 'auth')
  ],
  controllers: [AuthController],
  exports: [AuthService, JwtService],
})
export class AuthModule { }