import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
// import DatabaseCmsModule from '../../config/database.cms.module';
import { ConfigModule } from '@nestjs/config';
import { UserEntity } from '../../entities/auth/user.entity';
import { UserRepository } from '../../repositories/auth/user.repository';
import { UserService } from '../../service/auth/users.service';
import { RoleEntity } from 'src/entities/auth/role.entity';
import { UserRoleEntity } from 'src/entities/auth/user.role.entity';
import { UserController } from 'src/controller/auth/user.controller';

@Module({
  providers: [UserService, JwtService, UserRepository],
  imports: [JwtModule,
      TypeOrmModule.forFeature([UserEntity], 'auth')
  ],
  controllers: [UserController],
  exports: [UserService, TypeOrmModule],
})
export class UsersModule {}