import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ProductEntity } from "../../entities/shop/ProductEntity";
import { ProductController } from "../../controller/shop/product.controller";
import { ProductService } from "../../service/shop/product.service";
import { ProductRepository } from "../../repositories/shop/product.repository";

@Module({
  providers: [ProductService,ProductRepository],
  imports: [
      TypeOrmModule.forFeature([ProductEntity], 'shop')
  ],
  controllers: [ProductController],
  exports: [ProductService],
})
export class ProductModule {}