import { EntityRepository, Repository } from 'typeorm';
import { RegisterDto } from '../../dto/auth/RegisterDto';
import { RoleEntity } from 'src/entities/auth/role.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class RoleRepository extends Repository<RoleEntity> {
  constructor(
    @InjectRepository(RoleEntity,'auth') 
    private readonly repo : Repository<RoleEntity>
  ){
    super(RoleEntity,repo.manager,repo.queryRunner);
  }
  async findRoleGuest(): Promise<RoleEntity | undefined> {
    return await this.findOne({ where: { roleName: 'ROLE_GUEST' } });
  }
}