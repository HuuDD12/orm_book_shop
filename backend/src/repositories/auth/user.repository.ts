import { EntityRepository, Repository } from 'typeorm';
import { UserEntity } from '../../entities/auth/user.entity';
import { RegisterDto } from '../../dto/auth/RegisterDto';
import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserRepository extends Repository<UserEntity> {
  // Add custom methods here if needed
  constructor(
    @InjectRepository(UserEntity, 'auth') 
    private readonly userRepository : Repository<UserEntity>,
  ){super(UserEntity, userRepository.manager,userRepository.queryRunner)}
  async findOneUser(email:any){
    const user =  await this.createQueryBuilder("user")
    .leftJoinAndSelect("user.userRoles", "userRole")
    .leftJoinAndSelect("userRole.role", "role")
    .where("user.email = :email", { email })
    .getOne();
    const userWithRoleName = {
      id: user.id,
      avatar: user.avatar,
      firstName: user.firstName,
      lastName: user.lastName,
      age: user.age,
      date: user.date,
      address: user.address,
      isActive: user.isActive,
      email: user.email,
      password: user.password,
      // Lấy tên của vai trò từ kết quả truy vấn
      role: user.userRoles.map(userRole => userRole.role.roleName)
  };

  return userWithRoleName;
  }

  async createNew(dto: RegisterDto): Promise<UserEntity| undefined>{
    const user = this.create({
      firstName: dto.firstName,
      lastName: dto.lastName,
      email: dto.email,
      password: dto.password,
    });
    return await this.save(user);
  }
  
}