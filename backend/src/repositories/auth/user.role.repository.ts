import { EntityRepository, Repository } from 'typeorm';
import { UserRoleEntity } from 'src/entities/auth/user.role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserRoleRepository extends Repository<UserRoleEntity> {
  constructor(
    @InjectRepository(UserRoleEntity, 'auth')
    private readonly respository: Repository<UserRoleEntity>
  ) {
    super(UserRoleEntity, respository.manager, respository.queryRunner)
  }

  async saveRole(user: any,role:any) {
    const save = this.create({
      user: user,
      role: role
    });
    return await this.save(save);
  }

  async getOneByUser(user: any){
    return await this.findOne({where:{user: user}});
  }
}