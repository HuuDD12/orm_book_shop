import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { read } from "fs";
import { ProductEntity } from "src/entities/shop/ProductEntity";
import { Repository } from "typeorm";

@Injectable()
export class ProductRepository extends Repository<ProductEntity>{
    constructor(
        @InjectRepository(ProductEntity,'shop') 
        private readonly repo: Repository<ProductEntity>,
    ){
        super(ProductEntity,repo.manager,repo.queryRunner);
    }
    async getAll(){
        return this.find();
    }
}