import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from '../../dto/auth/user.dto';
import { UserRepository } from '../../repositories/auth/user.repository';
import { RegisterDto } from '../../dto/auth/RegisterDto';
import { LoginDto } from '../../dto/auth/LoginDto';
import * as bcrypt from 'bcrypt';
import { UserRoleRepository } from 'src/repositories/auth/user.role.repository';
import { RoleRepository } from 'src/repositories/auth/role.repository';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userRespository: UserRepository,
    private readonly userRoleRepository: UserRoleRepository,
    private readonly roleRepository: RoleRepository,
    private jwtService: JwtService,

  ) { }

  async login(dto: LoginDto) {
    const user = await this.userRespository.findOneUser(dto.email);
    if (user) {
      const match = await bcrypt.compare(dto.password, user.password)
      if (match) {
        const payload = {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          role: user.role,
        }
        const accessToken = await this.jwtService.signAsync(payload, { secret: process.env.JWT_SECRET });
        return{
          payload,
          accessToken,
        }
      } else {
        return {
          status: 404,
          mess: 'Sai mật khẩu',
        }
      }
    } else {
      return {
        status: 404,
        mess: 'Email không tồn tại'
      }
    }
  }
  async register(dto: RegisterDto) {
    if (! await this.userRespository.findOneUser(dto.email)) {
      const hash = await bcrypt.hash(dto.password, 10);
      dto.password = hash;
      const user = await this.userRespository.createNew(dto);
      const role = await this.roleRepository.findRoleGuest();
      return await this.userRoleRepository.saveRole(user, role);
    } else {
      return {
        status: 500,
        mess: 'Email đã tồn tại',
      }
    }
  }
}