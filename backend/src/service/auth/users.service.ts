// user.service.ts
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from '../../dto/auth/user.dto';
import { UserRepository } from '../../repositories/auth/user.repository';
// import { RoleService } from 'src/role/role.service';
// import { UserRoleService } from 'src/user-role/user-role.service';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    // private readonly userRoleService: UserRoleService,
    // private readonly roleService: RoleService,
  ) {}

  async create(dto: CreateUserDto, avatar?: string) {
    // Business logic for creating user
  }

  async findAll() {
    // Business logic for finding all users
  }

  async searchAll(q: any) {
    // Business logic for searching users
  }

  async findOne(id: number) {
    // Business logic for finding one user
  }

  async update(id: number, dto: UpdateUserDto) {
    // Business logic for updating user
  }

  remove(id: number) {
    // Business logic for removing user
  }
}
