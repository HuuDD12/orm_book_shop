import { Injectable } from "@nestjs/common";
import { ProductRepository } from "../../repositories/shop/product.repository";

@Injectable()
export class ProductService{
    constructor(
        private readonly repo : ProductRepository,
    ){}
    async getAll() {
        return await this.repo.getAll();
    }
}